<?php 
/*
 *  Author: Josh T. Reimer
	File: registrationhandler.php
	Date: February 7, 2020
	
	Description: 
	An php form handler to process
	user input from registration.php
	
 */
?>
<?php 
//error reporting
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

// global variables and constants
    $dbName = "Milestone 1";
    define('HOST_NAME' , "localHost");
    define('USER_NAME' , "root");
    define('PASSWORD' , "root");
    define('EMPTY_STRING' , "");

// check if "Submit" button was recieved
if (!isset($_POST['Submit'])) {
    die("Submission failed, no data received.");
}
else {
    // global variables and constants
    $email = $_POST['email'];
    $email = trim($email);
    $password = $_POST['password'];
    $password = trim($password);
    $firstName = $_POST['firstName'];
    $firstName = trim($firstName);
    $lastName = $_POST['lastName'];
    $lastName = trim($lastName);
    $streetAddress = $_POST['streetAddress'];
    $streetAddress = trim($streetAddress);
    $zipCode = $_POST['zipCode'];
    $zipCode = trim($zipCode);
    $state = $_POST['state'];
    $state = trim($state);
    $country = $_POST['country'];
    $country = trim($country);
    
    
}
$dbConnect = mysqli_connect(HOST_NAME, USER_NAME, PASSWORD);
if (!$dbConnect) {
    echo "<p>Connection Error: " . mysqli_connect_error() . "</p>";
}
else {
    //database selection
    if (mysqli_select_db($dbConnect, $dbName)) {
        //database is reached
        echo 
            "<p>Successfully reached " . $dbName . 
            " sql database.</p>";
        echo 
            "<p>" . $firstName .  " " . $lastName . "</p>";
            $tableName = "users";       
            $sql = "INSERT INTO $tableName (email, password, firstName,
             lastName, streetAddress, zipCode, state, country)
            VALUES('$email', '$password', '$firstName', '$lastName',
             '$streetAddress', '$zipCode', '$state', '$country')";
            if ($result = mysqli_query($dbConnect, $sql)) {
                echo 
                    "<p>Thanks my dude, you are now registered!</p>";
            }
            else {
                echo 
                    "<p>Error: " . mysqli_error($dbConnect) . 
                    "this one might be hard to fix</p>";
            }        
        }
    else {
        //error with database
        echo 
            "<p>Could not select the " . $dbName . 
            " database: " . mysqli_errno($dbConnect) . "</p>";
    }
    echo 
        "Database has closed";
    mysqli_close($dbConnect);
} 
    
?>